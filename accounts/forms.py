# from django import forms
# from .models import UserProfile

# class SignupForm(forms.ModelForm):
#     class Meta:
#         model = UserProfile
#         fields = ['username', 'password', 'date_of_birth']
#         widgets = {
#             'password': forms.PasswordInput()
#         }

from django import forms
from .models import CustomUser, Board , Pin

class SignupForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['username', 'password', 'date_of_birth']
        widgets = {
            'password': forms.PasswordInput(),
        }

class BoardForm(forms.ModelForm):
    class Meta:
        model = Board
        fields = ['name']

class PinForm(forms.ModelForm):
    class Meta:
        model = Pin
        fields = ['board', 'title', 'catagory', 'image', 'description']


