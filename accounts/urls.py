from django.urls import path
from . import views

urlpatterns = [
    path('', views.signup_view, name='signup'),
    path('success/',views.success,name='success'),
    path('login/', views.login_view, name='login'),
    path('dashboard/', views.dashboard_view, name='dashboard'),
    path('logout/', views.logout_view, name='logout'),
    path('board/', views.create_board, name='create_board'),
    path('create_pin/', views.create_pin, name='create_pin'),
    path('dashboard/pin/<int:id>/', views.pin_detail_view, name='pin'),

    path('follow/', views.follow_view, name='follow'),
    path('unfollow/', views.unfollow_view, name='unfollow'),

    path('like/', views.like_view, name='like'),
    path('dislike/', views.dislike_view, name='dislike'),
    path('comments/', views.comments_view, name='comment'),

    path('explore/', views.explore_view, name='explore'),

    path('search/', views.search_view, name='search')



    


]
