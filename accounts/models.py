from django.db import models
from django.contrib.auth.models import AbstractUser



# def user_directory_path(instance, filename):
#     return "user_{0}/{1}".format(instance.user.id, filename)


class CustomUser(AbstractUser):
    date_of_birth = models.DateField()

    def unfollow(self, user_to_unfollow):
        Follow.objects.filter(follower=self, followed=user_to_unfollow).delete()



class Follow(models.Model):
    follower = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='followers')
    followed = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='followed_users')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.followed.username}"







class Board(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


        
class Pin(models.Model):
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    catagory = models.CharField(max_length=255)  # Assuming you have a separate Tag model
    image = models.ImageField(upload_to='pins/')  # Image upload field
    description = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title 

    def save(self, *args, **kwargs):
        created = not self.pk  # Check if the pin is being created or updated
        super().save(*args, **kwargs)
        if created:
            PinLikes.objects.create(pin=self, likes=0)


class PinLikes(models.Model):
    pin = models.ForeignKey(Pin, on_delete=models.CASCADE, related_name='likes')
    likes = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"liked {self.pin.title}"


class PinUserLike(models.Model):
    pin = models.ForeignKey(Pin, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    def __str__(self):
        return f"User: {self.user.username}, Pin: {self.pin.title}"


# model for storing commennts
class Comment(models.Model):
    pin = models.ForeignKey(Pin, on_delete=models.CASCADE, related_name='comments')
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return f"Comment by {self.user.username} on Pin: {self.pin.title}"
