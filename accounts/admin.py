from django.contrib import admin
from django.contrib.admin.models import LogEntry
from .models import CustomUser,Board,Pin


admin.site.register(CustomUser)
admin.site.register(LogEntry)
admin.site.register(Board)
admin.site.register(Pin)