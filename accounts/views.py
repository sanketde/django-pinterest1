
from django.shortcuts import render, redirect,get_object_or_404
from django.contrib.auth import login, authenticate,logout
from .forms import SignupForm
from .models import CustomUser
from .forms import BoardForm, PinForm
from .models import Board, Pin, Follow,PinLikes,PinUserLike, Comment
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.db.models import Q
import json




def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST, request.FILES)
        if form.is_valid():
            # Create a new user account
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            date_of_birth = form.cleaned_data['date_of_birth']

            # Create the user with an encrypted password
            user = CustomUser(username=username, date_of_birth=date_of_birth)
            user.set_password(password)  # Set the encrypted password
            user.save()
            
            # Authenticate the user and log them in
            user = authenticate(request, username=username, password=password)
            login(request, user)
            
            return redirect('dashboard')  
    else:
        form = SignupForm()
    
    return render(request, 'signup.html', {'form': form})

def success(request):
    return render(request, 'success.html')

@login_required
def dashboard_view(request):
    username = request.user.username  
    pins = Pin.objects.all()
    user_boards = Board.objects.filter(user=request.user)
    context = {
        'pins': pins ,
        'username': username,
        'user_boards':user_boards,
        }
    return render(request, 'dashboard.html', context)  


def login_view(request):
    if request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('dashboard')  
        else:
            error_message = 'Invalid username or password'  
            return render(request, 'signup.html', {'error_message': error_message})
    else:
        return render(request, 'login.html')


@login_required
def logout_view(request):
    logout(request)
    return redirect('signup')



# dashboard page views

@login_required
def create_board(request):
    if request.method == 'POST':
        board_form = BoardForm(request.POST)
        if board_form.is_valid():
            board = board_form.save(commit=False)
            board.user = request.user
            board.save()
            return redirect('dashboard')
    else:
        board_form = BoardForm()

    context = {
        'form': board_form,
        'username': request.user.username
    }
    return render(request, 'create_board.html', context)



# create pin view

@login_required
def create_pin(request):
    if request.method == 'POST':
        pin_form = PinForm(request.POST, request.FILES)
        if pin_form.is_valid():
            pin = pin_form.save(commit=False)
            pin.board = get_board_object(request)  
            pin.save()
            return redirect('dashboard')
    else:
        pin_form = PinForm()

    user_boards = request.user.board_set.values_list('id', flat=True)
    context = {
        'pin_form': pin_form,
        'username': request.user.username,
        'user_boards': user_boards,
    }
    return render(request, 'create_pin.html', context)

def get_board_object(request):
    board_id = request.POST.get('board') 
    board = get_object_or_404(Board, id=board_id)
    return board


@login_required
def pin_detail_view(request, id):

    # to get all the details related to the given pin
    pin_detail = Pin.objects.select_related('board__user').get(id=id)

    # to find host name of a given pin
    host = pin_detail.board.user.username

     # to check user followed the pin host or not
    follower = request.user.followers.all()
    isHostFollowed = follower.filter(followed__username=host).exists()

    # to get all the boards of a loggedin user
    user_boards = Board.objects.filter(user=request.user)

    #  to find total likes in a given pin_id
    likes = PinLikes.objects.get(pin__id=id).likes

    # check the logged in user liked given pin or not
    isUserLiked = PinUserLike.objects.filter(pin_id=id, user=request.user).exists()

    # get all the comments for this pin
    comments = getComments(id)
    comment_data = []
    for comment in comments:
        username_part = comment.user.username.split("@")[0]
        comment_data.append((username_part, comment.content))

    context = {
        'pin_detail': pin_detail,
        'host' : host,
        'username':request.user,
        'isHostFollowed':isHostFollowed,
        'user_boards':user_boards,
        'likes':likes,
        'isUserLiked':isUserLiked,
        'comments': comment_data,
    }
    return render(request, 'pin_detail.html', context)



@login_required
def follow_view(request):
    if request.method == 'POST' and 'action' in request.POST and 'pin_host' in request.POST:
        action = request.POST['action']
        pin_host = request.POST['pin_host']
        # Perform follow logic based on the action and pin_id
        # ...
        Follow.objects.create(follower=request.user, followed=CustomUser.objects.get(username=pin_host))

        return JsonResponse({'status': 'success'})
    else:
        return JsonResponse({'status': 'error'})

@login_required
def unfollow_view(request):
    if request.method == 'POST' and 'action' in request.POST and 'pin_host' in request.POST:
        action = request.POST['action']
        pin_host = request.POST['pin_host']
        

        request.user.unfollow(CustomUser.objects.get(username=pin_host))

        return JsonResponse({'status': 'success'})
    else:
        return JsonResponse({'status': 'error'})




@login_required
def like_view(request):
    if request.method == 'POST':
        pin_id = request.POST['pin_id']

        # increment the likes for that pin and save it in database
        pin_likes = PinLikes.objects.get(pin_id=pin_id)
        pin_likes.likes += 1
        pin_likes.save()

        # update the table PinUserLike , that user had liked the pin
        pin = Pin.objects.get(id=pin_id)
        pin_user_like = PinUserLike(user=request.user, pin=pin)
        pin_user_like.save()

        #  to find total likes in a given pin_id
        likes = PinLikes.objects.get(pin__id=pin_id).likes

        return JsonResponse({'message': 'Pin liked successfully','likes':likes}, status=200)
    
    return JsonResponse({'message': 'Invalid request'}, status=400)




@login_required
def dislike_view(request):
    if request.method == 'POST':
        pin_id = request.POST['pin_id']
        
        # decrement the likes for that pin and save it in database
        pin_likes = PinLikes.objects.get(pin_id=pin_id)
        pin_likes.likes -= 1
        pin_likes.save()

        # remove the entry from  table PinUserLike , that user had liked the pin
        pin = Pin.objects.get(id=pin_id)
        PinUserLike.objects.filter(user=request.user, pin=pin).delete()

        #  to find total likes in a given pin_id
        likes = PinLikes.objects.get(pin__id=pin_id).likes

        return JsonResponse({'message': 'Pin liked successfully','likes':likes}, status=200)   
    return JsonResponse({'message': 'Invalid request'}, status=400)


# to get all the comments for a particular pin_id
@login_required
def comments_view(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        comment_message = data.get('comment')
        pin_id = data.get('pin_id')

        # insert the comment in comment table
        pin = Pin.objects.get(id=pin_id)
        comment = Comment(pin=pin, user=request.user, content=comment_message)
        comment.save()


        return JsonResponse({'message': 'Comment updated successfully'}, status=200)  
    return JsonResponse({'message': 'Invalid request'}, status=400)    
    # comments = Comment.objects.filter(pin_id=pin_id).order_by('created_at')
    # for comment in comments:
    #     print(f"Username: {comment.user.username}")
    #     print(f"Comment: {comment.content}")
    #     print("---")
    



# method to get all the comments for a given pin_id
def getComments(pin_id):
    comments = Comment.objects.filter(pin_id=pin_id).order_by('created_at')
    return comments


@login_required
def explore_view(request):
    username = request.user.username  

    # get the ids of followed users
    followed_users_ids = Follow.objects.filter(follower=request.user).values_list('followed', flat=True)

    # get the pins created by followed user
    pins = Pin.objects.filter(board__user__in=followed_users_ids)

    user_boards = Board.objects.filter(user=request.user)
    context = {
        'pins': pins ,
        'username': username,
        'user_boards':user_boards,
        }
    return render(request,"explore.html",context)


@login_required
def search_view(request):

    username = request.user.username  

    # get the search query from the logged in user
    query = request.GET.get('query', '')

    #filter the pins based on query
    pins = Pin.objects.filter(Q(title__icontains=query) | Q(catagory__icontains=query))

    user_boards = Board.objects.filter(user=request.user)


    context = {
        'pins': pins ,
        'username': username,
        'user_boards':user_boards,
        'query':query,
        }
    return render(request,"search.html", context)

