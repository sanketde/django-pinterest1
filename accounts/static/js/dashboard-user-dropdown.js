function toggleMenu(event) {
    event.stopPropagation(); // Prevents the click event from propagating to parent elements
    var menuContainer = document.getElementById("user-menu-container");
    menuContainer.classList.toggle("menu-open");

    // Add an event listener to the document to handle clicks outside of the user menu
    document.addEventListener("click", closeMenu);
}

function closeMenu(event) {
    var menuContainer = document.getElementById("user-menu-container");
    if (!menuContainer.contains(event.target)) {
        menuContainer.classList.remove("menu-open");
        document.removeEventListener("click", closeMenu);
    }
}